const fs = require('fs');

// Tu JSON inicial
let invitados = {
    "invitados": [{
        "id": 1,
        "nombre": "",
        "confirmo": 0,
        "fecha_confirmo": "",
        "contrasenia": "",
        "acompaniante": 0,
        "asistira": 0,
        "no_asistira": 0,
        "llego_salon": 0,
        "mesa": 1,
        "asiento": 1
    }]
};

// Tu lista de nombres
let nombres = [
    "JESSICA TAMEZ GARCIA",
    "JESSICA INVITADO",
    "DAVID RAMIREZ",
    "ARELY ALFARO",
    "ALEJANDRO PEREZ",
    "ALEJANDRO PEREZ INVITADO",
    "GABY PEREZ",
    "GABY PEREZ INVITADO",
    "JENNY RAMIREZ",
    "JENNY INVITADO",
    "SOFIA AFANADOR",
    "MARCO AFANADOR",
    "ROBERTO RODRIGUEZ",
    "ROBERTO INVITADO",
    "SANTIAGO RODRIGUEZ",
    "SAMUEL SIAS",
    "DIEGO FRANCO",
    "DIEGO INIVITADO",
    "MIRNA DIAZ",
    "MIRNA PAREJA",
    "ABI MEDINA",
    "ABI INVITADO",
    "KARINA CORONADO",
    "KARINA INVITADO",
    "NAYELY RIOS",
    "NAYELY INVITADO",
    "ROSENDO URESTI",
    "JAHIR RODRIGUEZ",
    "JAHIR INVITADO",
    "ARNOLDO MEDINA",
    "ARNOLDO INVITADO",
    "MIGUEL NORIEGA",
    "MIGUEL INVITADO",
    "ARLET CHENDO",
    "CORY",
    "DANIEL MARQUEZ",
    "DANIEL INVITADO",
    "ANGEL ESCAJEDA",
    "ANGEL INVITADO",
    "RAMON JAVALERA",
    "RAMON INVITADO",
    "EDUARDO ZAVALA",
    "EDUARDO INVITADO",
    "HECTOR GUAJARDO",
    "HECTOR INVITADO",
    "BERENICE RODRIGUEZ",
    "BERENICE INVITADO",
    "ANIRA GUARDADO",
    "ANIRA INVITADO",
    "SCARLETH SALAS",
    "SCARLETH INVITADO",
    "MARCO HERNANDEZ",
    "PAULINA OLIVARES",
    "VALERIA LARA",
    "IDALIA FLORES",
    "MARIO AFANADOR",
    "MARY FLORES",
    "ANGEL RODRIGUEZ",
    "LUIS OLIVARES",
    "LACHO DIMAS",
    "LACHO INVITADO",
    "MAMA SIAS",
    "CANDELARIO TAMEZ",
    "ANGELICA GARCIA",
    "BLANCA TAMEZ",
    "REYES PEREZ",
    "MAGUE TAMEZ",
    "JOVITA GUTIERREZ",
    "JULIA TAMEZ",
    "JUAN RAMIREZ",
    "NICOLAS ZUÑIGA",
    "PRICILA OLIVARES",
    "PRICILA INVITADO",
    "TIA PATY",
    "TIA CRISTY"

];

// Función para generar contraseña aleatoria
function generarContrasenia() {
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let contrasenia = '';
    for (let i = 0; i < 5; i++) {
        const randomIndex = Math.floor(Math.random() * caracteres.length);
        contrasenia += caracteres.charAt(randomIndex);
    }
    return contrasenia;
}

// Recorrer la lista de nombres y generar objetos de invitados
let id = 1;
for (let nombre of nombres) {
    let contrasenia = generarContrasenia();

    // Crear un nuevo objeto de invitado
    let nuevoInvitado = {
        "id": id++,
        "nombre": nombre,
        "confirmo": 0,
        "fecha_confirmo": "",
        "contrasenia": contrasenia,
        "acompaniante": 0,
        "asistira": 0,
        "no_asistira": 0,
        "llego_salon": 0,
        "mesa": 1,
        "asiento": 1
    };

    // Agregar el nuevo invitado a la lista
    invitados["invitados"].push(nuevoInvitado);
}

// Mostrar los invitados generados
console.log(invitados);

const jsonContent = JSON.stringify(invitados, null, 2);

fs.writeFile('invitadoss.json', jsonContent, 'utf8', (err) => {
    if (err) {
        console.error('Error al escribir el archivo:', err);
        return;
    }
    console.log('El archivo "invitados.json" ha sido creado exitosamente.');
});