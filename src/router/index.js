import Vue from 'vue'
import VueRouter from 'vue-router'
import Sesion from '../views/sesionView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'sesion',
    component: Sesion
  },
  {
    path: '/home',
    name: 'home',
    component: function () {
      return import('../views/HomeView.vue')
    }
  }, {
    path: '/confirmados',
    name: 'confirmados',
    component: function () {
      return import('../views/confirmados.vue')
    }
  }
]

const router = new VueRouter({
  routes
})

export default router
