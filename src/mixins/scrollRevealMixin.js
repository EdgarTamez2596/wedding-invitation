import ScrollReveal from 'scrollreveal';

export default {
    methods: {
        revealElement(selector, options) {
            const sr = ScrollReveal();
            sr.reveal(selector, options);
        },
    },
};